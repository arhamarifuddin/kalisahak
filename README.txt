=== Kalisahak ===
Contributors: Arham Arifuddin
Requires at least: WordPress 4.7
Tested up to: WordPress 4.7
Version: 1.1.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tags: istakalisa, akprind, aanimation, one-column, two-columns, right-sidebar, flexible-header, accessibility-ready, custom-colors, custom-header, custom-menu, custom-logo, editor-style, featured-images, footer-widgets, post-formats, rtl-language-support, sticky-post, theme-options, threaded-comments, translation-ready

== Description ==

Kalisahak is custom themes based by Twenty Seventeen. Twenty Seventeen brings your site to life with header video and immersive featured images. With a focus on business sites, it features multiple sections on the front page as well as widgets, navigation and social menus, a logo, and more. Personalize its asymmetrical grid with a custom color scheme and showcase your multimedia content with post formats. Our default theme for 2017 works great in many languages, for any abilities, and on any device.

[docs themes | https://codex.wordpress.org/Twenty_Seventeen]

== Installation ==

1. In your admin panel, go to Appearance -> Themes and click the 'Add New' button.
2. Type in Twenty Seventeen in the search form and press the 'Enter' key on your keyboard.
3. Click on the 'Activate' button to use your new theme right away.
4. Go to https://codex.wordpress.org/Twenty_Seventeen for a guide on how to customize this theme.
5. Navigate to Appearance > Customize in your admin panel and customize to taste.
