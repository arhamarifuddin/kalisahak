<?php
/**
 * Displays footer site info
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<div class="site-info">
	<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'kalisahak' ) ); ?>"><?php printf( __( 'Proudly powered by %s', 'kalisahak' ), 'WordPress' ); ?></a>
</div><!-- .site-info -->
